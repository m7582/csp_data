#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 20 17:22:13 2024

@author: MTI group
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from matplotlib.ticker import FormatStrFormatter
import os


def extract_title_from_filename(file_path):
    filename = os.path.basename(file_path).split('.')[0]  # Remove extension
    title = '_'.join(filename.split('_')[:2])  # Join first two parts
    return title


# Read MS CSV
csv_file_path_MS = 'MAMPUM03_MS.csv'
df_MS = pd.read_csv(csv_file_path_MS)
df_MS = df_MS.dropna(subset=['Relaxed_energy'])
MS_energy = df_MS['SP_energy']
MS_density = df_MS['Density']
MS_spgs = df_MS['Space_group_number']
MS_ID = df_MS['Structures']

# Read NC CSV
csv_file_path_NC = 'MAMPUM03_NC.csv'
df_NC = pd.read_csv(csv_file_path_NC)
df_NC = df_NC.dropna(subset=['Relaxed_energy'])
NC_energy = df_NC['SP_energy']
NC_density = df_NC['Density']
NC_spgs = df_NC['Space_group_number']
NC_ID = df_NC['Structures']

title = extract_title_from_filename(csv_file_path_MS)

# Sort the data
MS_energy, MS_density, MS_spgs, MS_ID = zip(*sorted(zip(MS_energy, MS_density, MS_spgs, MS_ID)))
NC_energy, NC_density, NC_spgs, NC_ID = zip(*sorted(zip(NC_energy, NC_density, NC_spgs, NC_ID)))

MS_energy = np.array(MS_energy)
MS_density = np.array(MS_density)
NC_energy = np.array(NC_energy)
NC_density = np.array(NC_density)

min_value = np.min([MS_energy.min(), NC_energy.min()])

MS_energy -= min_value
NC_energy -= min_value


# Uncomment this to test the cmap coloring
# def generate_random_array(length, min_val=0, max_val=30):
#     return np.random.randint(min_val, max_val+1, length).tolist() 

# desired_length = len(MS_energy)  
# num_matched_mol_MS = generate_random_array(desired_length)

# desired_length = len(NC_energy)  
# num_matched_mol_NC = generate_random_array(desired_length)


#df = pd.read_csv('similarity_csv_path_MS')
#num_matched_mol_MS = df['Number of Matched Molecules'].to_numpy()

#df = pd.read_csv('similarity_csv_path_NC')
#num_matched_mol_NC = df['Number of Matched Molecules'].to_numpy()

    
rc('font',**{'family':'serif','serif':['Kerkis']})
rc('text', usetex=True)
plt.rcParams['figure.figsize'] = [5, 4]
cm = plt.cm.get_cmap('coolwarm')

plt.rcParams.update({'font.size': 14})
fig, ax = plt.subplots(dpi=180)

           
sc = ax.scatter(x=MS_density,y=MS_energy, 
            s=25, marker='D', edgecolors='black',
            alpha=0.7, vmin=1, vmax=30, cmap=cm,
            lw=0.6)

sc_2 = ax.scatter(x=NC_density,y=NC_energy, 
            s=30, marker='o', alpha=0.7, edgecolors='black',  vmin=1, vmax=30, cmap=cm,  lw=0.6 )


ax.scatter(x=[],y=[], label='Monovalent salt',
            c='none',s=40, edgecolors='black',  lw=0.6, marker='D' )

ax.scatter(x=[],y=[],label='Neutral co-crystal',
            c='none',marker='o' ,s=45, edgecolors='black' , lw=0.6 )

ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
ax.set_ylabel(r' Relative energy ($ \rm eV$)')
ax.set_xlabel(r'Density ($\rm g/ cm^{3}$)')
ax.set_title(title)
ymin, ymax = ax.get_ylim()
xmin, xmax = ax.get_xlim()
cbar = plt.colorbar(sc)
ax.legend(loc='best', ncol=1, fontsize=13)
plt.show()     


