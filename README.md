# CSP for acid-base proton transfer ferroelectricty 

This document provides

 1) Instructions on the data from the manuscript
"Design of novel organic proton-transfer acid-base (anti-)ferroelectric salts with crystal
structure prediction".  [Paper_will_appear here.](https://www.researchgate.net/profile/Kristian-Berland). 

2) Details on how to reproduce the crystal structure prediction (CSP) calculations
acounting for different protonation states. 
Note: All the DFT calcualtions in VASP are based on the vdW-DF2 functional. 

# Data availability
## Molecules

DFT calculations for individual molecules are available in the NOMAD [NOMAD_single_molecules](https://nomad-lab.eu/prod/v1/gui/user/uploads/upload/id/g0u6ogcKSDKE419PxV0_7g) database. Each molecule has been geometry optimized in the gas phase.


## Benchmark systems

The paper described two benchmark systems, with CSD codes
MAMPUM03  and QUWZIS.
[MAMPUM03](https://nomad-lab.eu/prod/v1/gui/user/uploads/upload/id/g0u6ogcKSDKE419PxV0_7g)
and
[QUWZIS](https://nomad-lab.eu/prod/v1/gui/user/uploads/upload/id/UrJ_XV9WTSqAHh5sW25vxA). 
The naming scheme is the same as for the other acid-base systems (see below)

% ## Template calculation
% Initally, a set of CSP template calculations were done
%to investigate proton-affinity. 
% [Templates](https://nomad-lab.eu/prod/v1/gui/user/uploads/upload/id/g0u6ogcKSDKE419PxV0_7g)

## Crystal structures

Crystal structures generated by the CSP method, along with subsequent DFT calculations for the selected molecular combinations, are available in the NOMAD database. Each entry contains the following folders and files (exemplified by DCAN-TMPZ)

- DCAN-TMPZ
Subfolder
 - `MS`: Monovalent salt configuration, desirable for proton transfer ferroelectricity if packing supports proton-transfer chains 
-  `NC`: Neutral cocrystal configuration, i.e. possible paraelectric state if packing,  if packing supports proton-transfer chains 
-  `DS` (non listed for DCAN-TMPZ). The divalent salt configuration. 
  - `dioxy.xyz`: Coordinates of species 1 (acid)
  - `napht.xyz`: Coordinates of species 2 (base)
  - `napht.dioxy.mols`: Molecular axes used to align multipoles
  - `napht.dioxy.dma`: Atomic multipoles
  - `napht.dioxy_rank0.dma`: Atomic charges
  - `structures.csv`: A CSV file containing the generated structures ranked by force-field 
  energies 
   - `DCAN-TMPZ_MS_cif_files.zip`: Cif files of the 1000 lowest-energy Niggli-reduced structures of `structures.csv`. Further `.res`-files (SHELX format) for further structures not provided, but can be obtained upon request. 
  - `csp_dft1`: Folder containing the DFT calculations of the low-energy structures from the CSP landscape. In general, the first step is used for single-point calculations, while the final step is used for full relaxations. In some cases, only a single step is taken. In this case the folder is only used for single-point landscapes. 
  This is divided into the subfolder

In some cases the folder 
  - `csp_dft2`: is also listed. In this case, `csp_dft1` this output is used for single-point calculations. 


### DCAN-TMPZ
- NOMAD upload: [DCAN-TMPZ](https://nomad-lab.eu/prod/v1/gui/user/uploads/upload/id/LIAsyKM1REuXMlU0riwLrA)

Full relaxations in 
- `csp_dft2` 

### DCAN-PZ
- NOMAD upload: [DCAN-PZ](https://nomad-lab.eu/prod/v1/gui/user/uploads/upload/id/w4saEyNhQbSJNNNuiD2wTQ)


### TFHQ-TMPZ
- NOMAD upload: [TFHQ-TMPZ](https://nomad-lab.eu/prod/v1/gui/user/uploads/upload/id/oNCZD8SeQTiUG3ZkHzTLYQ)

### DFAN-DMPZ
- NOMAD upload: [DFAN-DMPZ](https://nomad-lab.eu/prod/v1/gui/user/uploads/upload/id/fCHDO6loQi-YZvPKB87eCw)


### DCAN-DMPZ
- NOMAD upload [DCAN-DMPZ](https://nomad-lab.eu/prod/v1/gui/user/uploads/upload/id/bWn31zJ7Tj6uFb8cI2tDvw)


### DFAN-TMPZ
- NOMAD upload [DFAN-TMPZ](https://nomad-lab.eu/prod/v1/gui/user/uploads/upload/id/jDMPCNsKT9GEPNQ4slVq7g)  
DS structures provided in  
- `DS` file provided.  

### TCHQ-DMPZ
- NOMAD upload [TCHQ-DMPZ](https://nomad-lab.eu/prod/v1/gui/user/uploads/upload/id/s9pgf6OKTb2WpRASBtvvJQ)
- TODO: Upload cif file. 

### DMAN-TMPZ
- NOMAD upload [DMAN-TMPZ](https://nomad-lab.eu/prod/v1/gui/user/uploads/upload/id/mYqOtSaqSDuTJbaTd2wr9g)

### TCHQ-TMPZ
- NOMAD upload [TCHQ-TMPZ](https://nomad-lab.eu/prod/v1/gui/user/uploads/upload/id/S0qtzuuPQ2aigqaVbOQvDg)

### TCHQ-PZ 
- NOMAD upload [TCHQ-PZ](https://nomad-lab.eu/prod/v1/gui/user/uploads/upload/id/UYvLP5YcQBWfZYuK9b-lxA)




# Python scripts

## Scripts for data visualization 
### 1. `plot_relaxed_landscape.py`
**Description:** This script plots the DFT-relaxed CSP landcape of a selected molecular combination.  
**Usage:** `NAME` should be set to the path of a desired system, i.e. `DFAN-PZ` and `ProtState` should be set i.e. `MS`. 

### 2. `violin_plot.py`
**Description:** This script that compares the distribution of low-energy CSP generated structures of MS versus NC using single-point DFT energies.  
**Usage:** Should be run in the CSVs folder (can be downloaded form repository).

## Scripts for data extraction
### 1. `extract_full_csp_data.py`
**Description:** This script extract single-point (SP) and fully relaxed DFT energies for the studies molecular combinations using both protonation states (MS and NC). 

**Usage:** Described in script.


# Computational procedure 
The CSP calculations were performed using the CSPY package, which is available at https://gitlab.com/mol-cspy/mol-cspy. 

## Input to the DMACRYS CSP calculations:  

Throughout, we have for "historical" reasons used the “dummy” name “napth” to denote the base species/molecules, and “dioxy” label to label acids species/molecule regardless of protonations state. The molecules are geometrically (DFT) optimized, and the output structure (CONTCAR in VASP format) should be converted to XYZ format, which can be used by the CSPY program.

To generate atomic multipoles (up to hexadecapole on each atom), atomic charges and molecular axes, the following command should be run after successfull installation of the CSPY program:

```bash
cspy-dma napht.xyz dioxy.xyz --charges "1 -1" --multiplicities "1 1" --use_psi4
```
In this example, we assume a single proton transfer between acid and base molecules. In the case of a neutral crystal, simply remove the --charges tag. The following files should be produced.

```bash
dioxy.xyz   #Coordinates of species 1 (acid) 
Napht.xyz   #Coordinates of species 2 (base) 
napht.dioxy.mols #Molecular axes used to align multipoles    
napht.dioxy.dma  #Atomic multipoles 
napht.dioxy_rank0.dma #Atomic charges
```

After producing the input files, a CSP calculation can be performed as follows,

```bash
mpiexec csp napht.xyz dioxy.xyz  -c napht.dioxy_rank0.dma -m napht.dioxy.dma -a napht.dioxy.mols -g ferro_common_spgs
```
Where ferro_common_spgs contains the information of the space group to generate quasi-random trial crystal structures and the requested number of valid (i.e. successfully lattice energy minimised) structures. We have defined this in the cspy-git/cspy/configurations.py file in the CSPY program directory. More specifically, for each system we asked the program to generate 20000 valid structures for the each of the follwoing space groups: 14, 15, 19, 1, 25, 26, 27, 28, 29, 2, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 3, 40, 41, 42, 43, 44, 45, 46, 4, 5, 61, 6, 7, 8, 9, 60, 96, 76, 145, 146, 160, 56, 13, 169, 170, 88, 20, 86, 148. Following quasi-random structure generation, each crystal structure is lattice energy minimised in the rigid-molecule approximation, with intermolecular interactions described by an empirical exp-6 repulsion-dispersion model combined with the electrostatic model from the cspy-dma calculation: atomic partial charges are applied in the initial lattice energy minimisation, followed by re-optimisation with atomic multipole electrostatics. Force field-based lattice energy calculations were performed using the DMACRYS software.

### Output of CSP (classic) 

The following files can be found in the working directory of CSP calculations for each of the acid-base combinations,

```bash
napht.dioxy_[space_group_number].db   # Databases that contains the infomration for generated structure 
                                      # for each of the space groups.      
```

The follwoing CSPY command will find redundant structures within all the database files, combine the unique structures into a new database file (defaulting to output.db), then find unique structures within the combined file.
```bash
cspy-db cluster *.db
```
Finally, the following command will extract the structures into a zip file and the related information into a CSV file.
```bash
cspy-db dump output.db
```
And the output files are as follows,
```bash
structures.zip   #Structures evaluated with force fields.     
structures.csv       #Energetic ranking at the force-field DMACRYS level.  
```
In the structures.zip file, we have the generated random structures in the .res (SHELX) format, which are ranked based on the DMACRYS (atomic multipole-based force field) energies. 

## DFT calculatios for the generated crystal strutcures

As detailed in the Methods section of the paper, the 100 lowest-energy structures from the DMACRYS landscape were re-ranked using single-point DFT/vdW-DF2 calculations. Additionally, full DFT relaxation was performed for a subset of these molecular combinations.

### Preparing .RES structures for DFT calculations

Some of the structures have unnecessary big unit cells. The following command is used to reduced the unit cells to the Niggli cells. The reduced cells will be in the .CIF format. 
```bash
python standardize.py *.res
```








