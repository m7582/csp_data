#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  2 18:53:18 2024

"""

import os
import pandas as pd
import numpy as np
from ase.io import read
import spglib
from ase.build import separate

def get_density(atoms):
    density = np.sum(atoms.get_masses()) * 1.66054 / atoms.get_volume()
    return density

def get_space_group_number(atoms):
    cell = atoms.get_cell()
    positions = atoms.get_scaled_positions()
    atomic_numbers = atoms.get_atomic_numbers()
    symmetry_dataset = spglib.get_symmetry_dataset((cell, positions, atomic_numbers))
    spacegroup_number = symmetry_dataset['number']
    return spacegroup_number

def process_structures(relaxed_base_directory, sp_base_directory, output_csv,
                       cocrys_form_eng=False, system=None):
    
    monocrys_dic = {'DMAN': 'SAPBAM', 'DFAN': 'FLUANA',  'TMPZ': 'MPYRAZ02',
                    'PZ': 'PYRAZI01', 'DMPZ': 'WIZFEQ01', 'TCHQ': 'PECBII' }
    
    # Relaxed structures
    relaxed_folders = [d for d in os.listdir(relaxed_base_directory) 
                       if os.path.isdir(os.path.join(relaxed_base_directory, d)) 
                       and (d.startswith("napht") or d.startswith("dioxy"))]
    
    # SP structures
    sp_folders = [d for d in os.listdir(sp_base_directory) 
                  if os.path.isdir(os.path.join(sp_base_directory, d)) 
                  and (d.startswith("napht") or d.startswith("dioxy"))]

    data = {}
    
    if cocrys_form_eng:
        acid_mol = system.split('_')[0]  
        base_mol = system.split('_')[1]  
        acid_path = os.path.join(monocrys_dir, monocrys_dic[acid_mol])
        base_path = os.path.join(monocrys_dir, monocrys_dic[base_mol])
        
        contcar_path_acid = os.path.join(acid_path, 'CONTCAR')
        outcar_path_acid = os.path.join(acid_path, 'OUTCAR')
        
        contcar_path_base = os.path.join(base_path, 'CONTCAR')
        outcar_path_base = os.path.join(base_path, 'OUTCAR')
        
        atoms_acid = read(contcar_path_acid)
        outcar_acid = read(outcar_path_acid)
        mols_acid = separate(atoms_acid)
        num_mol_acids = len(mols_acid)
        total_energy_acid = outcar_acid.get_total_energy() / num_mol_acids
        
        atoms_base = read(contcar_path_base)
        outcar_base = read(outcar_path_base)
        mols_base = separate(atoms_base)
        num_mol_base = len(mols_base)
        total_energy_base = outcar_base.get_total_energy() / num_mol_base
        
        for folder in relaxed_folders:
            folder_path = os.path.join(relaxed_base_directory, folder)
            contcar_path = os.path.join(folder_path, 'CONTCAR')
            outcar_path = os.path.join(folder_path, 'OUTCAR')

            try:
                atoms = read(contcar_path)
                outcar = read(outcar_path)
                mols = separate(atoms)
                number_of_molecules = len(mols) / 2
                total_energy = outcar.get_total_energy() / number_of_molecules
                cocrys_formation_energy = total_energy - total_energy_base - total_energy_acid
                density = get_density(atoms)
                space_group_number = get_space_group_number(atoms)
                
                data[folder] = data.get(folder, {})
                data[folder]["CC_energy"] = cocrys_formation_energy
                data[folder]["Relaxed_energy"] = total_energy
                data[folder]["Density"] = density
                data[folder]["Space_group_number"] = space_group_number
            except Exception as e:
                print(f"An error occurred processing {folder_path}: {e}")
    
    else:
        # Process directories for relaxed structures
        for folder in relaxed_folders:
            folder_path = os.path.join(relaxed_base_directory, folder)
            contcar_path = os.path.join(folder_path, 'CONTCAR')
            outcar_path = os.path.join(folder_path, 'OUTCAR')

            try:
                atoms = read(contcar_path)
                outcar = read(outcar_path)
                mols = separate(atoms)
                number_of_molecules = len(mols) / 2
                total_energy = outcar.get_total_energy() / number_of_molecules
                density = get_density(atoms)
                space_group_number = get_space_group_number(atoms)

                data[folder] = data.get(folder, {})
                data[folder]["Relaxed_energy"] = total_energy
                data[folder]["Density"] = density
                data[folder]["Space_group_number"] = space_group_number
            except Exception as e:
                print(f"An error occurred processing {folder_path}: {e}")
        

    # Process directories for SP structures
    for folder in sp_folders:
        folder_path = os.path.join(sp_base_directory, folder)
        outcar_path = os.path.join(folder_path, 'OUTCAR')
        contcar_path = os.path.join(folder_path, 'CONTCAR')

        try:
            atoms = read(contcar_path)
            outcar_sp = read(outcar_path, index=0)
            mols = separate(atoms)  
            number_of_molecules = len(mols) / 2
            total_energy_sp = outcar_sp.get_total_energy() / number_of_molecules
            density = get_density(atoms)
            space_group_number = get_space_group_number(atoms)

            data[folder] = data.get(folder, {})
            data[folder]["SP_energy"] = total_energy_sp
        except Exception as e:
            print(f"An error occurred processing {folder_path}: {e}")
    


    # Convert to DataFrame
    df = pd.DataFrame.from_dict(data, orient='index')
    df.index.name = 'Structures'
    
    print(df)

    # Define columns to keep based on available data
    columns = [col for col in ['Relaxed_energy', 'SP_energy', 'Density', 'Space_group_number', 'CC_energy'] if col in df]
    # Sort the DataFrame by Relaxed_energy if it exists, otherwise use any available column
    sort_column = 'Relaxed_energy' if 'Relaxed_energy' in df else columns[0]
    df.sort_values(by=sort_column, inplace=True)
    
    # Save to CSV
    df.to_csv(output_csv)

# Usage
PATH = '.'  # Update to path to NOMAD downloads.
NAME = 'DCAN-TMPZ'  # change to appropriate system. 
ProtState = 'MS'   # protonation state. Either MS or NC (or for some systems DS) 

base_directory = os.path.join(PATH, NAME, ProtState) 

csp_df1 = os.path.join(base_directory, 'csp_df1')
csp_df2 = os.path.join(base_directory, 'csp_df2')

if os.path.isdir(csp_df1) and not os.path.isdir(csp_df2):
    relaxed_base_directory = sp_base_directory = csp_df1
elif os.path.isdir(csp_df1) and os.path.isdir(csp_df2):
    sp_base_directory = csp_df1
    relaxed_base_directory = csp_df2
else:
    raise Exception("Both csp_df1 and csp_df2 directories are missing")

CSV_path = '.'  # Update to path to desired CSV directory

output_csv = os.path.join(CSV_path, NAME + '_' + ProtState + '.csv') 

monocrys_dir = '/CSVs/monocrystals'

# Use cocrys_form_eng flag to calculate and add cocrystal fomration energy column
process_structures(relaxed_base_directory, sp_base_directory, output_csv, cocrys_form_eng=False, system='DFAN_TMPZ')

