#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 20 17:34:24 2024

@author: MTI group
"""

import numpy as np
import pandas as pd
import argparse
from pathlib import Path
from ccdc.crystal import PackingSimilarity
from ccdc.io import CrystalReader


def get_num_match_mols(crys_1_cif, crys_2_cif):
    sim = PackingSimilarity()
    sim.settings.distance_tolerance = 0.3
    sim.settings.angle_tolerance = 30.0
    sim.settings.ignore_hydrogen_positions = True
    sim.settings.allow_molecular_differences = True
    sim.settings.ignore_bond_types = True
    sim.settings.allow_artificial_inversion = True
    sim.settings.packing_shell_size = 20
    sim.settings.nmolecules = 30

    crys1 = CrystalReader(crys_1_cif)[0]
    crys2 = CrystalReader(crys_2_cif)[0]
    com = sim.compare(crys1, crys2)

    nmatch_mol = com.nmatched_molecules
    RMSD = com.rmsd

    return nmatch_mol, RMSD


def similarity_check(exp_structure, csp_generated_structure):
    num_matched_mol, rmsd = get_num_match_mols(exp_structure, csp_generated_structure)
    return num_matched_mol, rmsd


def process_and_save_to_csv(exp_structure, csp_directory, csp_rankings, output_csv):
    # Load the existing CSV into a DataFrame
    df = pd.read_csv(csp_rankings)

    # Initialize results list
    results = []

    # Loop through structures and compute similarity check
    for csp_struc in df['Structures'][0:num]:                                       
        csp_structure = csp_directory + '/' +  csp_struc + '.cif'                                      
        num_matched_mol, rmsd = similarity_check(exp_structure, csp_structure)      
        results.append((num_matched_mol, rmsd))                          
        print(csp_struc, num_matched_mol)                                           

    # Convert results to a DataFrame
    results_df = pd.DataFrame(results, columns=['num_mol_match', 'RMSD'])

    # Append new columns to the existing DataFrame
    df['num_mol_match'] = results_df['num_mol_match']
    df['RMSD'] = results_df['RMSD']

    # Save the updated DataFrame back to a CSV file
    df.to_csv(output_csv) #, index=False)


num = 70

exp_structure = 'MAMPUM03.cif'
#csp_rankings = 'MAMPUM03_MS.csv'
#csp_directory = 'MS'
csp_directory = 'NC'
csp_rankings = 'MAMPUM03_NC.csv'

output_csv =  csp_rankings.split('.')[0] + '_sim_exp.csv'

process_and_save_to_csv(exp_structure, csp_directory, csp_rankings, output_csv)

