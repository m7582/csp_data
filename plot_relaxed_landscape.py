#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  3 21:55:31 2024

@author: MTI group
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from matplotlib.ticker import FormatStrFormatter
import pandas as pd
import os

# List of MS space groups
polar_spsgroups = [1, 3, 4, 5, 6, 7, 8, 9, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35,
                36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 75, 76, 77, 78, 79, 80,
                99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 143, 144,
                145, 146, 157, 156, 158, 159, 160, 161, 168, 169, 170, 171, 172, 173,
                183, 184, 185, ]

def extract_title_from_filename(file_path):
    filename = os.path.basename(file_path).split('.')[0]  # Remove extension
    title = '_'.join(filename.split('_')[:2])  # Join first two parts
    return title

# Read MS CSV
csv_file_path_MS = 'TCHQ_TMPZ_MS_csp.csv'
df_MS = pd.read_csv(csv_file_path_MS)
MS_energy = df_MS['Relaxed_energy']
MS_density = df_MS['Density']
MS_spgs = df_MS['Space_group_number']
MS_ID = df_MS['Structures']

# Read NC CSV
csv_file_path_NC = 'TCHQ_TMPZ_NC_csp.csv'
df_NC = pd.read_csv(csv_file_path_NC)
NC_energy = df_NC['Relaxed_energy']
NC_density = df_NC['Density']
NC_spgs = df_NC['Space_group_number']
NC_ID = df_NC['Structures']

# Extract title from CSV filename
title = extract_title_from_filename(csv_file_path_MS)

# Sort the data
MS_energy, MS_density, MS_spgs, MS_ID = zip(*sorted(zip(MS_energy, MS_density, MS_spgs, MS_ID)))
NC_energy, NC_density, NC_spgs, NC_ID = zip(*sorted(zip(NC_energy, NC_density, NC_spgs, NC_ID)))

MS_energy = np.array(MS_energy)
MS_density = np.array(MS_density)
NC_energy = np.array(NC_energy)
NC_density = np.array(NC_density)

min_value = np.min([MS_energy.min(), NC_energy.min()])

MS_energy -= min_value
NC_energy -= min_value

# Feature Packing Lists
feature_packs = {
    "TCHQ_TMPZ": ([True, False, False, False, False, False, True, True, True, False] + [False]*(len(MS_ID) - 10),
                  [True]*(len(NC_ID))),
    "TCHQ_DMPZ": ([False]*len(MS_ID), [True]*len(NC_ID)),
    "DCAN_DMPZ": ([True]*7 + [False]*(len(MS_ID)-7), [True]*len(NC_ID)),
    "DCAN_PZ": ([False, False, False, False, False, True, False, False, False, False] + [False]*10,
                [True]*20),
    "DMAN_TMPZ": ([True, True, False, False, True, False, False, False, False, False],
                  [False, False, True] + [False]*7),
    "DFAN_TMPZ": ([True, True, True, True, True, True, True, True, False, False] + [False]*10,
                  [False] + [True]*19),
    "DFAN_DMPZ": ([True]*len(MS_ID), [False]*len(NC_ID)),
    "DFAN_PZ": ([True] + [False]*(len(MS_ID) - 1), [True, True, True, False] + [True]*(len(NC_ID) - 4))
}

# Determine which feature pack to use
if title.startswith("TCHQ_TMPZ"):
    TCHQ_TMPZ_FE_pack_MS, TCHQ_TMPZ_FE_pack_NC = feature_packs["TCHQ_TMPZ"]
elif title.startswith("TCHQ_DMPZ"):
    TCHQ_TMPZ_FE_pack_MS, TCHQ_TMPZ_FE_pack_NC = feature_packs["TCHQ_DMPZ"]
elif title.startswith("DCAN_DMPZ"):
    TCHQ_TMPZ_FE_pack_MS, TCHQ_TMPZ_FE_pack_NC = feature_packs["DCAN_DMPZ"]
elif title.startswith("DCAN_PZ"):
    TCHQ_TMPZ_FE_pack_MS, TCHQ_TMPZ_FE_pack_NC = feature_packs["DCAN_PZ"]
elif title.startswith("DMAN_TMPZ"):
    TCHQ_TMPZ_FE_pack_MS, TCHQ_TMPZ_FE_pack_NC = feature_packs["DMAN_TMPZ"]
elif title.startswith("DFAN_TMPZ"):
    TCHQ_TMPZ_FE_pack_MS, TCHQ_TMPZ_FE_pack_NC = feature_packs["DFAN_TMPZ"]
elif title.startswith("DFAN_DMPZ"):
    TCHQ_TMPZ_FE_pack_MS, TCHQ_TMPZ_FE_pack_NC = feature_packs["DFAN_DMPZ"]
elif title.startswith("DFAN_PZ"):
    TCHQ_TMPZ_FE_pack_MS, TCHQ_TMPZ_FE_pack_NC = feature_packs["DFAN_PZ"]
else:
    raise ValueError("Unknown feature pack title prefix")

# Create masks
polar_mask = [True if i in polar_spsgroups else False for i in MS_spgs]
polar_mask_NC = [True if i in polar_spsgroups else False for i in NC_spgs]

polar_mask_inv = [not x for x in polar_mask]
polar_mask_NC_inv = [not x for x in polar_mask_NC]

# Plot settings
rc('font', **{'family':'serif', 'serif':['Kerkis']})
rc('text', usetex=True)
plt.rcParams['figure.figsize'] = [6, 5]
plt.rcParams.update({'font.size': 14})
fig, ax = plt.subplots(dpi=400)

# Plot scatter
ax.scatter(
    x=MS_density[polar_mask],
    y=MS_energy[polar_mask],
    s=30, facecolors='#61AFF0', marker='D', alpha=0.8
)
ax.scatter(
    x=NC_density[polar_mask_NC],
    y=NC_energy[polar_mask_NC],
    s=35, marker='o', facecolors='#61AFF0', alpha=0.8
)
ax.scatter(
    x=MS_density[polar_mask_inv],
    y=MS_energy[polar_mask_inv],
    s=30, facecolors='None', marker='D', alpha=0.8
)
ax.scatter(
    x=NC_density[polar_mask_NC_inv],
    y=NC_energy[polar_mask_NC_inv],
    s=35, marker='o', facecolors='None', alpha=0.8
)

# Add dummy plot for legend
dummy_plot, = plt.plot([], [], c='#61AFF0',  alpha=0.8,  lw=5, label='Polar spacegroup')

# Additional scatter plots
ax.scatter(
    x=MS_density, y=MS_energy,
    c='none', s=30, lw=.8, edgecolors='black', marker='D', label='Monovalent salt'
)
ax.scatter(
    x=NC_density, y=NC_energy,
    c='none', s=35, marker='o', lw=.8, edgecolors='black', label='Neutral co-crystal'
)
ax.scatter(
    x=MS_density[TCHQ_TMPZ_FE_pack_MS], y=MS_energy[TCHQ_TMPZ_FE_pack_MS],
    facecolors='none', edgecolors='#B51D14', s=100, ls='-', lw=0.5, label='Connected PT path'
)
ax.scatter(
    x=NC_density[TCHQ_TMPZ_FE_pack_NC], y=NC_energy[TCHQ_TMPZ_FE_pack_NC],
    facecolors='none', edgecolors='#B51D14', s=100, ls='-', lw=0.5
)

# Axis labels
ax.set_ylabel(r' Relative energy ($\rm eV$)')
ax.set_xlabel(r'Density ($\rm g/cm^{3}$)')

# Set plot title
ax.set_title(title)

ymin, ymax = ax.get_ylim()
xmin, xmax = ax.get_xlim()

# Legend
ax.legend(loc='upper left', ncol=1)

# Set plot limits and format
plt.ylim(-0.01, 0.2)
ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))

# Show plot
plt.show()