#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  6 12:01:35 2024

@author: mojtaba
"""

import pandas as pd
import os
import matplotlib.pyplot as plt
from matplotlib import rc
import seaborn as sns
from collections import defaultdict


# Directory containing the CSV files
directory = os.getcwd()

systems_data = defaultdict(lambda: {'MS': [], 'NC': [], 'all_energies': []})

# First pass: Collecting all energies
for filename in os.listdir(directory):
    if filename.endswith(".csv"):
        # Extract the system name and protonation state from the filename
        parts = filename.split('_')
        system_name = '_'.join(parts[:2])
        protonation_state = parts[2]
        
        # Construct the full file path
        filepath = os.path.join(directory, filename)

        # Read the CSV file
        df = pd.read_csv(filepath)
        
        # Extract the SP_energy column
        sp_energies = df['SP_energy'].tolist()
        
        # Store the energies in the respective system and state
        systems_data[system_name][protonation_state].extend(sp_energies)
        systems_data[system_name]['all_energies'].extend(sp_energies)

# Second pass: Normalizing energies and collecting results
data = []

for system_name, energy_data in systems_data.items():
    # Find the lowest SP_energy across all protonation states for this system
    min_sp_energy = min(energy_data['all_energies'])
    
    # Normalize energies and append the results
    for protonation_state, energies in energy_data.items():
        if protonation_state != 'all_energies':
            normalized_energies = [energy - min_sp_energy for energy in energies]
            for sp_energy in normalized_energies:
                data.append([sp_energy, protonation_state, system_name])

# Create a DataFrame from the collected data
df = pd.DataFrame(data, columns=['SP_energy', 'Protonation_state', 'System_name'])

protonation_map = {
    'MS': 'Monovalent salt',
    'NC': 'Neutral co-crystal'
}
df['Protonation_state'] = df['Protonation_state'].map(protonation_map)

# Custom order for systems
ordered_systems = [
    'DMAN_TMPZ', 'TFHQ_TMPZ', 'DFAN_TMPZ', 'DFAN_DMPZ', 'DFAN_PZ',
    'TCHQ_TMPZ', 'TCHQ_DMPZ', 'TCHQ_PZ', 'DCAN_DMPZ', 'DCAN_PZ', 'DCAN_DFPZ'
]

# Update the 'System_name' column to category type and specify the order
df['System_name'] = pd.Categorical(df['System_name'], categories=ordered_systems, ordered=True)



df_filtered = df[df['SP_energy'] <= 10]

# Plotting
rc('font',**{'family':'serif','serif':['Kerkis']})
rc('text', usetex=True)
plt.rcParams['figure.figsize'] = [6, 5]
plt.rcParams.update({'font.size': 15})
fig, ax = plt.subplots(dpi=120)


sns.violinplot(data=df_filtered, x='System_name', y='SP_energy', hue='Protonation_state',
               split=True, inner=None, linewidth=.8, cut=0, bw=0.4)

ax.set_ylabel(r' Relative energy ($ \rm eV$)', fontsize=16)
ax.set_xlabel(None)
plt.xticks(rotation=80, size=14)
plt.yticks(size=14)
plt.legend(loc="upper left")
plt.tight_layout()
plt.savefig('violin2.png')
plt.show()
